package st.rhapsody.ld36.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.*;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.level.Zone;

/**
 * Created by nicklas on 29/08/16.
 */
public class GameOverScreen implements Screen{

    private SpriteBatch spriteBatch;

    private OrthographicCamera orthographicCamera;
    private Animation animation;
    private float stateTime;
    private BitmapFont font;
    private GlyphLayout layout;

    @Override
    public void show() {

        orthographicCamera = new OrthographicCamera(320, 240);

        spriteBatch = new SpriteBatch();
        layout = new GlyphLayout();
        font = new BitmapFont(Gdx.files.internal("font.fnt"), Gdx.files.internal("font.png"), false);


        //LD36.audio.ld36_titlesong.loop(0.5f);
    }

    private void clearScreen() {
        Color zoneColor = Zone.Type.FOREST.getZoneColor();
        Gdx.gl.glClearColor(zoneColor.r, zoneColor.g, zoneColor.b, zoneColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void render(float delta) {
        clearScreen();


        spriteBatch.setProjectionMatrix(orthographicCamera.combined);
        spriteBatch.begin();

        layout.setText(font, "G A M E  O V E R");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-30, (240/2));




        spriteBatch.end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            LD36.changeToTitleScreen();
        }
    }

    @Override
    public void resize(int width, int height) {
        orthographicCamera.position.set(320/2,240/2,0);
        orthographicCamera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        LD36.audio.ld36_titlesong.stop();
    }

    @Override
    public void dispose() {

    }
}
