package st.rhapsody.ld36.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.*;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.level.Zone;

/**
 * Created by nicklas on 29/08/16.
 */
public class InstructionsScreen implements Screen{

    private SpriteBatch spriteBatch;

    private OrthographicCamera orthographicCamera;
    private Animation animation;
    private float stateTime;
    private BitmapFont font;
    private GlyphLayout layout;

    @Override
    public void show() {

        orthographicCamera = new OrthographicCamera(320, 240);

        spriteBatch = new SpriteBatch();
        layout = new GlyphLayout();
        font = new BitmapFont(Gdx.files.internal("font.fnt"), Gdx.files.internal("font.png"), false);


        LD36.audio.ld36_titlesong.loop();
    }

    private void clearScreen() {
        Color zoneColor = Zone.Type.FOREST.getZoneColor();
        Gdx.gl.glClearColor(zoneColor.r, zoneColor.g, zoneColor.b, zoneColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void render(float delta) {
        clearScreen();


        spriteBatch.setProjectionMatrix(orthographicCamera.combined);
        spriteBatch.begin();

        layout.setText(font, "Y O U   A R E  T R A P P E D  I N  A N C I E N T  T I M E S");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-120, (240/2)+115);

        layout.setText(font, "T H R E E  B E A S T S  T O O K  T H E  P A R T S");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-100, (240/2)+100);


        layout.setText(font, "Y O U  N E E D  F O R  Y O U R  U F O  T O  G E T  B A C K");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-118, (240/2)+85);

        layout.setText(font, "A L S O  L O O K  O U T  F O R  P O W E R F U L  W E A P O N S");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-125, (240/2)+60);


        layout.setText(font, "W A S D  T O  M O V E");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-40, (240/2));


        layout.setText(font, "1 2 3  T O  C H A N G E  W E A P O N");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-70, (240/2)-15);

        layout.setText(font, "S P A C E  T O  A T T A C K");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-50, (240/2)-30);


        layout.setText(font, "G O O D  L U C K");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-30, (240/2)-75);



        spriteBatch.end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            LD36.changeToGameScreen();
        }
    }

    @Override
    public void resize(int width, int height) {
        orthographicCamera.position.set(320/2,240/2,0);
        orthographicCamera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        LD36.audio.ld36_titlesong.stop();
    }

    @Override
    public void dispose() {

    }
}
