package st.rhapsody.ld36.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.level.Zone;

/**
 * Created by nicklas on 29/08/16.
 */
public class EndScreen implements Screen{

    private SpriteBatch spriteBatch;

    private OrthographicCamera orthographicCamera;
    private Animation animation;
    private float stateTime;
    private BitmapFont font;
    private GlyphLayout layout;

    @Override
    public void show() {

        orthographicCamera = new OrthographicCamera(320, 240);

        spriteBatch = new SpriteBatch();
        layout = new GlyphLayout();
        font = new BitmapFont(Gdx.files.internal("font.fnt"), Gdx.files.internal("font.png"), false);


        TextureAtlas heroAtlas = LD36.textures.heroAtlas;
        String[] animations = new String[]{"dance_1","dance_2","dance_3","dance_4","dance_5","dance_6","dance_7","dance_8"};

        Array<TextureRegion> frames = new Array<TextureRegion>();

        for (String animation : animations) {
            TextureAtlas.AtlasRegion region = heroAtlas.findRegion(animation);
            frames.add(region);
        }

        animation = new Animation(0.15f, frames);

        //LD36.audio.ld36_song.loop(0.5f);
    }

    private void clearScreen() {
        Color zoneColor = Zone.Type.CAVES.getZoneColor();
        Gdx.gl.glClearColor(zoneColor.r, zoneColor.g, zoneColor.b, zoneColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void render(float delta) {
        clearScreen();


        stateTime += delta;

        TextureRegion keyFrame = animation.getKeyFrame(stateTime, true);

        spriteBatch.setProjectionMatrix(orthographicCamera.combined);
        spriteBatch.begin();


        for (int x = -12; x < 320; x += 24) {
            spriteBatch.draw(keyFrame,x,0, 40, 40);
        }

        for (int x = -12; x < 320; x += 24) {
            spriteBatch.draw(keyFrame,x,200, 40, 40);
        }




        layout.setText(font, "Y O U  D I D  I T");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-30, (240/2)+40);

        layout.setText(font, "N O W  Y O U  N E E D  T O  E S C A P E");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-70, (240/2));

        layout.setText(font, "F R O M  T H I S  A N C I E N T");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-55, (240/2)-15);

        layout.setText(font, "C O M P U T E R  T E C H N O L O G Y");
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, layout, (320/2)-65, (240/2)-30);

        spriteBatch.end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            LD36.changeToTitleScreen();
        }
    }

    @Override
    public void resize(int width, int height) {
        orthographicCamera.position.set(320/2,240/2,0);
        orthographicCamera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        LD36.audio.ld36_song.stop();
    }

    @Override
    public void dispose() {

    }
}
