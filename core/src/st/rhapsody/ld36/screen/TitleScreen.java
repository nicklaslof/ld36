package st.rhapsody.ld36.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.level.Zone;

/**
 * Created by nicklas on 29/08/16.
 */
public class TitleScreen implements Screen{

    private SpriteBatch spriteBatch;

    private OrthographicCamera orthographicCamera;
    private Animation animation;
    private float stateTime;
    private BitmapFont font;
    private GlyphLayout layout;

    @Override
    public void show() {

        orthographicCamera = new OrthographicCamera(320, 240);


        spriteBatch = new SpriteBatch();
        layout = new GlyphLayout();
        font = new BitmapFont(Gdx.files.internal("font.fnt"), Gdx.files.internal("font.png"), false);




        TextureAtlas heroAtlas = LD36.textures.heroAtlas;
        String[] animations = new String[]{"dance_1","dance_2","dance_3","dance_4","dance_5","dance_6","dance_7","dance_8"};

        Array<TextureRegion> frames = new Array<TextureRegion>();

        for (String animation : animations) {
            TextureAtlas.AtlasRegion region = heroAtlas.findRegion(animation);
            frames.add(region);
        }

        animation = new Animation(0.15f, frames);


        LD36.audio.ld36_titlesong.loop();
    }

    private void clearScreen() {
        Color zoneColor = Zone.Type.FOREST.getZoneColor();
        Gdx.gl.glClearColor(zoneColor.r, zoneColor.g, zoneColor.b, zoneColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void render(float delta) {
        clearScreen();

        stateTime += delta;

        TextureRegion keyFrame = animation.getKeyFrame(stateTime, true);

        spriteBatch.setProjectionMatrix(orthographicCamera.combined);
        spriteBatch.begin();
        spriteBatch.draw(keyFrame,0,0, 40, 40);
        spriteBatch.draw(keyFrame,0,200, 40, 40);
        spriteBatch.draw(keyFrame,288,200, 40, 40);
        spriteBatch.draw(keyFrame,288,0, 40, 40);






        layout.setText(font, "U  F  O     Q  U  E  S  T");
        font.getData().setScale(1.f);
        font.draw(spriteBatch, layout, (320/2)-75, (240/2)+120);


        layout.setText(font, "C R E A T E D  B Y");
        font.getData().setScale(0.7f);
        font.draw(spriteBatch, layout, (320/2)-75, (240/2)+70);


        layout.setText(font, "N I C K L A S  L O F");
        font.getData().setScale(0.7f);
        font.draw(spriteBatch, layout, (320/2)-60, (240/2)+40);

        layout.setText(font, "A N D");
        font.getData().setScale(0.7f);
        font.draw(spriteBatch, layout, (320/2)-20, (240/2)+20);

        layout.setText(font, "M A T H I A S  L I N D F E L D T");
        font.getData().setScale(0.7f);
        font.draw(spriteBatch, layout, (320/2)-95, (240/2));


        layout.setText(font, "F O R  L U D U M  D A R E  3 6");
        font.getData().setScale(0.7f);
        font.draw(spriteBatch, layout, (320/2)-90, (240/2)-50);

        layout.setText(font, "A U G U S T  2 0 1 6");
        font.getData().setScale(0.7f);
        font.draw(spriteBatch, layout, (320/2)-60, (240/2)-70);

        layout.setText(font, "A N C I E N T   T E C H N O L O G Y");
        font.getData().setScale(0.7f);
        font.draw(spriteBatch, layout, (320/2)-100, (240/2)-109);







        spriteBatch.end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            LD36.changeToInstructions();
        }
    }

    @Override
    public void resize(int width, int height) {
        orthographicCamera.position.set(320/2,240/2,0);
        orthographicCamera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        LD36.audio.ld36_titlesong.stop();
    }

    @Override
    public void dispose() {

    }
}
