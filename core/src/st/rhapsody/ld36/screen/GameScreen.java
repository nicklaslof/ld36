package st.rhapsody.ld36.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import st.rhapsody.ld36.gui.InventoryGui;
import st.rhapsody.ld36.gui.ScoreGui;
import st.rhapsody.ld36.level.Level;
import st.rhapsody.ld36.level.Room;

/**
 * Created by nicklas on 27/08/16.
 */
public class GameScreen implements Screen{
    private SpriteBatch spriteBatch;

    private OrthographicCamera orthographicCamera;

    private SpriteBatch guiSpriteBatch;
    private ScoreGui scoreGui;
    private InventoryGui inventoryGui;
    private Level level;
    private GlyphLayout layout;
    private BitmapFont font;
    private String message;
    private float messageCountDown;

    @Override
    public void show() {

        orthographicCamera = new OrthographicCamera(320, 240);


        spriteBatch = new SpriteBatch();
        guiSpriteBatch = new SpriteBatch();




        level = new Level();

        boolean generated;

        do{
            generated = level.generate();
        }while (!generated);

        inventoryGui = new InventoryGui(level);
        scoreGui = new ScoreGui(level);

        layout = new GlyphLayout();
        font = new BitmapFont(Gdx.files.internal("font.fnt"), Gdx.files.internal("font.png"), false);

    }

    @Override
    public void render(float delta) {
        clearScreen();
        tick(delta);
        renderLevel();
        renderGui();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(0,0,0,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    private void renderGui() {
        guiSpriteBatch.setProjectionMatrix(orthographicCamera.combined);
        guiSpriteBatch.begin();
        scoreGui.render(guiSpriteBatch);
        inventoryGui.render(guiSpriteBatch);
        guiSpriteBatch.end();
    }

    private void renderLevel() {
        spriteBatch.setProjectionMatrix(orthographicCamera.combined);
        spriteBatch.begin();

        level.render(spriteBatch);

        if (level.hasMessage()){
            message = level.getMessage();
            level.clearMessage();
            messageCountDown = 3.0f;
        }


        if (message != null && messageCountDown > 0) {

            layout.setText(font, message);

            font.getData().setScale(0.5f);
            font.setColor(1,1,1,0.6f);
            font.draw(spriteBatch, layout, (320 / 2)-100, (240 / 2));
        }else{
            message = "";
            messageCountDown = 0.0f;
        }


        spriteBatch.end();
    }

    private void tick(float delta) {
        level.tick(delta);
        inventoryGui.tick(delta);
        scoreGui.tick(delta);
        if (messageCountDown > 0.0f) {
            messageCountDown -= delta;
        }
    }

    @Override
    public void resize(int width, int height) {




        orthographicCamera.position.set(320/2,240/2,0);
        orthographicCamera.update();

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
