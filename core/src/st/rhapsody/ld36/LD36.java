package st.rhapsody.ld36;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import st.rhapsody.ld36.resource.Audio;
import st.rhapsody.ld36.resource.Textures;
import st.rhapsody.ld36.screen.*;

public class LD36 extends Game{
	public static Textures textures;
	public static Audio audio;
	private static Screen currentScreen;
	private static boolean changeToGameScreen;
	private static boolean changeToInstructions;
	private static boolean changeToGameOver;
	private static boolean changeToTitleScreen;
	private static boolean changeToEndScreen;

	@Override
	public void create() {




		this.textures = new Textures();

		this.audio = new Audio();


		currentScreen = new TitleScreen();

		setScreen(currentScreen);
	}

	public static void changeToInstructions(){
		changeToInstructions = true;
	}

	public static void changeToGameScreen(){
		changeToGameScreen = true;
	}

	public static void changeToGameOverScreen(){
		changeToGameOver = true;
	}

	public static void changeToTitleScreen(){
		changeToTitleScreen = true;
	}

	public static void changeToEndScreen(){
		changeToEndScreen = true;
	}

	@Override
	public void render() {
		super.render();
		if (changeToGameScreen){
			setScreen(new GameScreen());
			changeToGameScreen = false;
		}else if (changeToInstructions){
			setScreen(new InstructionsScreen());
			changeToInstructions = false;
		}else if (changeToGameOver){
			setScreen(new GameOverScreen());
			changeToGameOver = false;
		}else if (changeToTitleScreen){
			setScreen(new TitleScreen());
			changeToTitleScreen = false;
		}else if (changeToEndScreen){
			setScreen(new EndScreen());
			changeToEndScreen = false;
		}
	}
}
