package st.rhapsody.ld36.level;

import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.entity.behaviour.*;

import java.util.Map;

/**
 * Created by nicklas on 28/08/16.
 */
public class ZonePopulater {
    public static void populate(Zone.Type type, Level level, Map<Zone.Type, Array<Room>> map) {
        Array<Room> rooms = map.get(type);

        switch(type){
            case PLAINS: {
                Room randomRoom = rooms.random();
                randomRoom.addEntity(LD36.textures.weaponsAtlas, "axe", new PickupBehaviour(level, AxeBehaviour.class), 16, 16);


                randomRoom = getValidRoom(rooms);
                randomRoom.addEntity(LD36.textures.snakeAtlas, null, new BossBehaviour(level,25, "compass",20), "snake_1", new String[]{"snake_1", "snake_2"}, new String[]{"snake_1", "snake_2"}, 30, 30, 0,0);

                for (int i = 0; i < rooms.size*4; i++) {
                    randomRoom = getValidRoom(rooms);
                    randomRoom.addEntity(LD36.textures.scorpionAtlas, null, new EnemyBehaviour(level,15, 1), "scorpion_1",
                            new String[]{"scorpion_1", "scorpion_2", "scorpion_3", "scorpion_4"},
                            new String[]{"scorpion_1", "scorpion_2", "scorpion_3", "scorpion_4"}, 10, 10,0,0);

                }


                break;
            }

            case FOREST: {
                Room randomRoom = getValidRoom(rooms);
                randomRoom.addEntity(LD36.textures.weaponsAtlas, "spear", new PickupBehaviour(level, SpearBehaviour.class), 16, 16);
                randomRoom = getValidRoom(rooms);
                randomRoom.addEntity(LD36.textures.mammothAtlas, null, new BossBehaviour(level,35, "steeringwheel", 20), "mammoth_1",
                        new String[]{"mammoth_1", "mammoth_2"}, new String[]{"mammoth_1", "mammoth_2"}, 30, 30, 0,0);



                for (int i = 0; i < rooms.size*4; i++) {
                    randomRoom = getValidRoom(rooms);
                    randomRoom.addEntity(LD36.textures.foxAtlas, null, new EnemyBehaviour(level,25, 2), "fox_1",
                            new String[]{"fox_1", "fox_2", "fox_3", "fox_4", "fox_5", "fox_6", "fox_7", "fox_8"},
                            new String[]{"fox_1", "fox_2", "fox_3", "fox_4", "fox_5", "fox_6", "fox_7", "fox_8"}, 10, 10,0,0);

                }

                break;
            }


            case CAVES: {
                Room randomRoom = getValidRoom(rooms);
                randomRoom.addEntity(LD36.textures.weaponsAtlas, "slingshot", new PickupBehaviour(level, SlingshotBehaviour.class), 16,16);
                randomRoom = getValidRoom(rooms);
                randomRoom.addEntity(LD36.textures.sabreAtlas, null, new BossBehaviour(level,45, "map", 20), "sabre_1",
                        new String[]{"sabre_1", "sabre_2"}, new String[]{"sabre_1", "sabre_2"}, 30, 30, 0,0);


                randomRoom = getValidRoom(rooms);
                randomRoom.addEntity(LD36.textures.ufoAtlas, null, new UfoBehaviour(level), "ufo_1",
                        new String[]{"ufo_1", "ufo_2","ufo_3", "ufo_4"}, new String[]{"ufo_1", "ufo_2","ufo_3", "ufo_4"}, 32, 32, 0,0);

                for (int i = 0; i < rooms.size*4; i++) {
                    randomRoom = getValidRoom(rooms);
                    randomRoom.addEntity(LD36.textures.batAtlas, null, new EnemyBehaviour(level,35, 4), "bat_1",
                            new String[]{"bat_1", "bat_2", "bat_3", "bat_4"},
                            new String[]{"bat_1", "bat_2", "bat_3", "bat_4"}, 10, 10,0,0);

                }
                break;
            }
        }


    }

    public static Room getValidRoom(Array<Room> rooms) {
        Room room;
        boolean validRoom = true;
        do{
            room = rooms.random();

            if (room.isSpawnProtected()){
                validRoom = false;
            }else if (!room.hasTiles()){
                validRoom = false;
            }else{
                validRoom = true;
            }

        }while(!validRoom);

        return room;
    }
}
