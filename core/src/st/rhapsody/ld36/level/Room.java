package st.rhapsody.ld36.level;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.entity.Entity;
import st.rhapsody.ld36.entity.behaviour.*;

/**
 * Created by nicklas on 27/08/16.
 */
public class Room {

    public static int TILE_WIDTH = 20;
    public static int TILE_HEIGHT = 14;
    private final int roomx;
    private final int roomy;

    public static int counter = 0;
    private final int roomCounter;

    private boolean spawnProtected = false;

    private Tile[] tiles;
    private Array<Entity> entities = new Array<Entity>();

    private Array<Entity> tempArray = new Array<Entity>();

    private Array<Entity> tempArray2 = new Array<Entity>();



    private static Sprite backgroundSprite = new Sprite(LD36.textures.solidTexture);

    public Zone.Type zone;
    private Level level;
    public boolean northExit;
    public boolean southExit;
    public boolean westExit;
    public boolean eastExit;
    private int cost = 0;
    private Object enities;

    public Room(Zone.Type zone, Level level, boolean northExit, boolean southExit, boolean westExit, boolean eastExit, int roomx, int roomy) {
        this.zone = zone;
        this.level = level;
        this.northExit = northExit;
        this.southExit = southExit;
        this.westExit = westExit;
        this.eastExit = eastExit;
        this.roomx = roomx;
        this.roomy = roomy;

        regenerate();

        counter++;
        this.roomCounter = counter;
    }

    public void setSpawnProtected(boolean spawnProtected) {
        this.spawnProtected = spawnProtected;
    }

    public boolean isSpawnProtected() {
        return spawnProtected;
    }

    public void regenerate(){
        calculateCost();
        generateTiles();
        generateEntites();
    }

    private void generateEntites() {
        entities = new Array<Entity>(TILE_WIDTH*TILE_HEIGHT);
    }
    public void addEntity(TextureAtlas texture, String regionName, Behaviour behaviour, int width, int height){
        addEntity(texture, regionName, behaviour, null, null, null, width, height, 0, 0);
    }

    public void addEntityAtPosition(TextureAtlas texture, String regionName, Behaviour behaviour, int width, int height, float x, float y) {
        addEntity(texture, regionName, behaviour, null, null, null, width, height, x, y);
    }

    public void addEntity(TextureAtlas texture, String regionName, Behaviour behaviour, String idle, String[] animations, String[] upAnimations, int width, int height, float x, float y) {
        boolean placed = false;
        boolean animated = true;
        int loopCounter = 0;

        TextureAtlas.AtlasRegion textureRegion = null;

        if (regionName != null){
            textureRegion = texture.findRegion(regionName);
        }

        if (idle == null && animations == null && upAnimations == null){
            animated = false;
        }

        while (!placed && loopCounter < 20){
            loopCounter++;
            if (x == 0 && y == 0 || loopCounter > 1) {
                x = MathUtils.random(1, TILE_WIDTH - 1) * 16;
                y = MathUtils.random(1, TILE_HEIGHT - 1) * 16;

                if (behaviour instanceof BossBehaviour || behaviour instanceof UfoBehaviour) {
                    x = (TILE_WIDTH / 2) * 16;
                    y = (TILE_HEIGHT / 2) * 16;
                }
            }


            if (getTileAt((int)x/16, (int)y/16) == null || !getTileAt((int)x/16, (int)y/16).isCollidable()) {
                try {
                    Entity entity;

                    if (animated){
                        entity = new Entity(x, y, texture, idle, animations, upAnimations, width, height);
                    }else{
                        entity = new Entity(x, y, textureRegion);
                    }

                    entity.setBehaviour(behaviour);
                    entities.add(entity);
                   //System.out.println("Added " + behaviour + " " + texture+ " at " + x + " " + y);
                    placed = true;
                }catch (Exception ex){

                }
            }
        }
    }


    private Tile getTileAt(int x, int y){
        Tile tile = tiles[y * TILE_WIDTH + x];
        return tile;
    }

    private void generateTiles() {
        tiles = new Tile[(TILE_WIDTH*TILE_HEIGHT)*2];
        for (int x = 0; x < TILE_WIDTH; x++) {
            for (int y = 1; y < TILE_HEIGHT; y++) {
                GENERATE: if ((x == 0 || x == TILE_WIDTH-1) || (y == 1 || y == TILE_HEIGHT -1 )) {

                    if (northExit){
                        if ( y == TILE_HEIGHT - 1 && x > 7 && x < 12){
                            break GENERATE;
                        }
                    }

                    if (southExit){
                        if ( y == 1 && x > 7 && x < 12){
                            break GENERATE;
                        }
                    }

                    if (westExit){
                        if ( y > 5 && y < 9 && x == 0){
                            break GENERATE;
                        }
                    }


                    if (eastExit){
                        if ( y > 5 && y < 9 && x == TILE_WIDTH -1){
                            break GENERATE;
                        }
                    }

                    tiles[y * TILE_WIDTH + x] = new Tile(new Sprite(LD36.textures.solidTexture), x, y, zone.getDetailColor(), true);
                }

                int random = MathUtils.random(1, 100);

                TextureAtlas atlas = null;

                switch(zone){
                    case CAVES:
                        atlas = LD36.textures.cavesAtlas;
                        break;
                    case FOREST:
                        atlas = LD36.textures.forestAtlas;
                        break;
                    case PLAINS:
                        atlas = LD36.textures.plainsAtlas;
                }


                if (random < 5){
                    if (tiles[y * TILE_WIDTH + x] == null) {
                        TextureAtlas.AtlasRegion atlasRegion = atlas.getRegions().get(random);
                        Sprite sprite = new Sprite(atlasRegion);
                        tiles[y * TILE_WIDTH + x] = new Tile(sprite, x, y, zone.getDetailColor(), false);
                    }
                }
            }
        }
    }

    private void calculateCost() {
        this.cost = 0;
        if (northExit) cost++;
        if (southExit) cost++;
        if (westExit) cost++;
        if (eastExit) cost++;
    }

    public void tick(float delta){

        if (tempArray.size > 0){
            tempArray.clear();
        }

        for (Tile tile : tiles) {
            if (tile != null) {
                tile.tick(delta);
            }
        }


        tempArray2.clear();
        tempArray2.addAll(entities);

        for (Entity entity : entities) {
            if (entity != null) {
                if (entity.isDisposed()){
                    tempArray.add(entity);
                    break;
                }
                entity.tick(delta);

                for (Entity e : tempArray2) {
                    if (!e.equals(entity)) {
                        if (entity.getRectangle().overlaps(e.getRectangle())) {
                            entity.collided(e);
                        }
                    }
                }
            }

            if (entity.getRectangle().overlaps(level.getPlayer().getRectangle())){
                entity.collided(level.getPlayer());
            }
        }

        for (Entity entity : tempArray) {
            entities.removeValue(entity,false);
        }
    }

    public void render (SpriteBatch spriteBatch){
        for (int x = 0; x < TILE_WIDTH; x++) {
            for (int y = 1; y < TILE_HEIGHT; y++) {
                Tile tile = tiles[y * TILE_WIDTH + x];
                backgroundSprite.setColor(zone.getZoneColor());
                backgroundSprite.setPosition(x*16, y*16);
                backgroundSprite.draw(spriteBatch);
                if (tile != null) {
                    tile.render(spriteBatch);
                }
            }
        }


        for (Entity entity : entities) {
            if (entity != null) {
                entity.render(spriteBatch);
            }
        }

    }

    public Tile[] getTiles() {
        return tiles;
    }

    public Direction whichRoomChangeDirection(Entity entity) {

        Vector2 position = entity.getPosition();
        int x = (int) (position.x/16);
        int y = (int) (position.y/16);

        if (northExit && (y == TILE_HEIGHT  && x > 7 && x < 12)){
            return Direction.NORTH;
        }

        if (southExit && (y == 0  && x > 7 && x < 12)){
            return Direction.SOUTH;
        }

        if (westExit && (y > 5 && y < 9 && x == -1 )){
            return Direction.WEST;
        }


        if (eastExit && (y > 5 && y < 9 && x == TILE_WIDTH )){
            return Direction.EAST;
        }

        return Direction.NONE;
    }

    public int getCost() {
        return cost;
    }

    public int getRoomx() {
        return roomx;
    }

    public int getRoomy() {
        return roomy;
    }

    public int getCounter() {
        return roomCounter;
    }

    public void addConnections(Connections connections) {
        if (connections.north){
            northExit = true;
        }

        if (connections.south){
            southExit = true;
        }

        if (connections.east){
            eastExit = true;
        }

        if (connections.west){
            westExit = true;
        }

        regenerate();
    }

    public Zone.Type getZone() {
        return zone;
    }

    public boolean hasTiles() {
        int counter = 0;
        for (Tile tile : tiles) {
            if (tile != null){
                counter++;
            }
        }

        return counter !=0;
    }

    public Array<Entity> getEntities() {
        return entities;
    }
}