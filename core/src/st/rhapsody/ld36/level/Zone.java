package st.rhapsody.ld36.level;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;

import java.util.Map;

/**
 * Created by nicklas on 27/08/16.
 */
public class Zone {

    public enum Type{
        PLAINS(generateColor(154, 135, 30), generateColor(113, 99, 23)),
        FOREST(generateColor(62, 116, 10), generateColor(18, 77, 15)),
        CAVES(generateColor(96, 110, 119), generateColor(63, 66, 74));

        private Color zoneColor;
        private Color detailColor;

        Type(Color zoneColor, Color detailColor) {
            this.zoneColor = zoneColor;
            this.detailColor = detailColor;
        }

        public Color getZoneColor() {
            return zoneColor;
        }

        public Color getDetailColor() {
            return detailColor;
        }
    }

    public static Color generateColor(float r, float g, float b){
        return new Color(r/255f, g/255f, b/255f, 1);
    }

}
