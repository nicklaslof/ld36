package st.rhapsody.ld36.level;

import com.badlogic.gdx.math.MathUtils;

import static st.rhapsody.ld36.utils.Shuffle.shuffle;

import java.util.ArrayList;

/**
 * Created by nicklas on 27/08/16.
 */
public class Connections {

    public boolean north;
    public boolean south;
    public boolean west;
    public boolean east;

    int cost;

    ArrayList<Boolean> randomDirections = new ArrayList<Boolean>(4);

    public Connections(int maxConnections) {

        for (int i = 0; i < 4; i++) {
            randomDirections.add(MathUtils.randomBoolean(0.5f));
        }

        int totalConnections = 0;
        shuffle(randomDirections);

        ADD:
        {
            north = randomDirections.get(0);
            if (north){
                totalConnections++;
            }

            if (totalConnections >= maxConnections){
                break ADD;
            }

            south = randomDirections.get(1);
            if (south){
                totalConnections++;
            }

            if (totalConnections >= maxConnections){
                break ADD;
            }

            west = randomDirections.get(2);
            if (west){
                totalConnections++;
            }

            if (totalConnections >= maxConnections){
                break ADD;
            }

            east = randomDirections.get(3);
            if (east){
                totalConnections++;
            }

            if (totalConnections >= maxConnections){
                break ADD;
            }

        }


        generateCost();
    }

    public Connections(Connections connections) {
        if (connections.north){
            south = true;
        }

        if (connections.south){
            north = true;
        }

        if (connections.east){
            west = true;
        }

        if (connections.west){
            east = true;
        }

        generateCost();
    }

    public void generateCost() {
        if (north) cost++;
        if (south) cost++;
        if (west) cost++;
        if (east) cost++;
    }

    public int getCost(){
        return cost;
    }

}
