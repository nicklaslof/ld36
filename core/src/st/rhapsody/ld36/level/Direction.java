package st.rhapsody.ld36.level;

/**
 * Created by nicklas on 27/08/16.
 */
public enum Direction {

    NORTH,
    SOUTH,
    EAST,
    WEST,
    NONE
}
