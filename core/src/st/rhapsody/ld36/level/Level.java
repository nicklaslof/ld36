package st.rhapsody.ld36.level;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.entity.Entity;
import st.rhapsody.ld36.entity.behaviour.*;

import java.util.*;

/**
 * Created by nicklas on 27/08/16.
 */
public class Level {
    public static int LEVEL_WIDTH = 100;
    public static int LEVEL_HEIGHT = 100;

    private Room[] rooms;

    public static Random random = new Random();

    private Room currentRoom;


    private Entity player;

    private Map<Zone.Type, Array<Room>> map = new HashMap<Zone.Type, Array<Room>>();
    private boolean gameFinished;
    private String message;


    public Level() {

    }



    public boolean generate() {
        //System.out.println("Generating level");
        try {

            map = new HashMap<Zone.Type, Array<Room>>();

            int x = LEVEL_WIDTH / 2;
            int y = LEVEL_HEIGHT / 2;

            rooms = new Room[(LEVEL_WIDTH * LEVEL_HEIGHT)];
            generateZone(Zone.Type.PLAINS, x, y, null);
            Room roomForZoneConnection = findRoomForZoneConnection(Zone.Type.PLAINS);
            generateZone(Zone.Type.FOREST, 0, 0, roomForZoneConnection);
            roomForZoneConnection = findRoomForZoneConnection(Zone.Type.FOREST);
            generateZone(Zone.Type.CAVES, 0, 0, roomForZoneConnection);



            //System.out.println("PLAINS: " + map.get(Zone.Type.PLAINS).size + "   FOREST: " + map.get(Zone.Type.FOREST).size + "   CAVE: " + map.get(Zone.Type.CAVES).size);

            if (map.get(Zone.Type.PLAINS).size != 7 || map.get(Zone.Type.FOREST).size != 7 || map.get(Zone.Type.CAVES).size != 7){
                return false;
            }
            for (Zone.Type type : Zone.Type.values()) {
                ZonePopulater.populate(type, this, map);
            }



            int requiredBosses = 3;
            int requiredPickups = 3;
            int requiredUfo = 1;

            int bosses = 0;
            int pickups = 0;
            int ufo = 0;


            for (Array<Room> roomArray : map.values()) {
                for (Room room : roomArray) {
                    Array<Entity> entities = room.getEntities();
                    for (Entity entity : entities) {
                        if (entity != null && entity.getBehaviour() != null){
                            Behaviour behaviour = entity.getBehaviour();
                            if (behaviour.getClass().getName().equals(PickupBehaviour.class.getName())){
                                pickups++;
                            }
                            if (behaviour.getClass().getName().equals(BossBehaviour.class.getName())){
                                bosses++;
                            }
                            if (behaviour.getClass().getName().equals(UfoBehaviour.class.getName())){
                                ufo++;
                            }
                        }
                    }
                }
            }


            //System.out.println("  "+bosses+"   "+ pickups+"   "+ufo);

            if (bosses < requiredBosses || pickups < requiredPickups || ufo < requiredUfo){
                return false;
            }


            currentRoom = rooms[y * LEVEL_WIDTH + x];


            player = new Entity(320 / 2, 240 / 2, LD36.textures.heroAtlas, "idle",
                    new String[]{"walk_1", "walk_2", "walk_3", "walk_4", "walk_5", "walk_6", "walk_7", "walk_8"},
                    new String[]{"walk_up1", "walk_up2", "walk_up3", "walk_up4"}, 20, 20
            ).setBehaviour(new PlayerBehaviour(this));

            player.setPlayer(true);
        }catch (Exception ex){
            return false;
        }

        return true;
    }

    private Room findRoomForZoneConnection(Zone.Type zone) {
        for (Room room : rooms) {
            if (room != null && room.getZone().equals(zone)){
                return room;
            }
        }
        return null;
    }

    private Room getRoomAt(int x, int y){
        return rooms[y * LEVEL_WIDTH + x];
    }

    private void generateZone(Zone.Type zone, int x, int y, Room roomForZoneConnection) {
        //System.out.println("Generating "+zone.name());
        //int numberToCreate = MathUtils.random(8,15);
        int numberToCreate = 8;

        ArrayList<Room> rooms = new ArrayList<Room>();

        generateRoom(zone,x, y, rooms, numberToCreate, roomForZoneConnection);



    }

    private void generateRoom(Zone.Type zone, int x, int y, ArrayList<Room> previousGeneratedRooms, int globalCost, Room roomForZoneConnection) {
        //System.out.println(globalCost+" "+previousGeneratedRooms.size());



        Array<Room> zoneRooms = map.get(zone);
        if (zoneRooms == null){
            zoneRooms = new Array<Room>();
            map.put(zone, zoneRooms);
        }

        Connections zoneConnection = null;

        if (roomForZoneConnection != null){

            boolean foundValidExit = false;

            Connections connections = null;

            while (!foundValidExit){
                foundValidExit = true;
                connections = generateConnections(1);
                validateConnections(connections,roomForZoneConnection.getRoomx(), roomForZoneConnection.getRoomy());
                roomForZoneConnection.addConnections(connections);
                if (roomForZoneConnection.getCost() < 2){
                    foundValidExit = false;
                }else{
                    zoneConnection = new Connections(connections);
                    globalCost += 1;
                }
            }


            if (connections.north){
                x = roomForZoneConnection.getRoomx();
                y = roomForZoneConnection.getRoomy()+1;
            }else if (connections.south){
                x = roomForZoneConnection.getRoomx();
                y = roomForZoneConnection.getRoomy()-1;
            }else if (connections.east){
                x = roomForZoneConnection.getRoomx()+1;
                y = roomForZoneConnection.getRoomy();
            }else if (connections.west){
                x = roomForZoneConnection.getRoomx()-1;
                y = roomForZoneConnection.getRoomy();
            }


        }

        if (!isFirstRun(x,y) && previousGeneratedRooms.isEmpty()){
            return;
        }

        if (isFirstRun(x, y)){
            Connections connections = generateConnections(globalCost);
            Room room = new Room(zone, this, connections.north, connections.south, connections.west, connections.east, x, y);
            if (zoneConnection != null){
                room.addConnections(zoneConnection);
            }else{
                room.setSpawnProtected(true);
            }
            rooms[y * LEVEL_WIDTH + x] = room;
            zoneRooms.add(room);
            globalCost -= room.getCost();
            previousGeneratedRooms.add(room);

        }else{
            //shuffle(previousGeneratedRooms);
            ArrayList<Room> toRemove = new ArrayList<Room>();
            ArrayList<Room> toAdd = new ArrayList<Room>();
            GENERATE:
            for (Room previousGeneratedRoom : previousGeneratedRooms) {
                //System.out.println(previousGeneratedRoom);
                if (previousGeneratedRoom.northExit){
                    if (rooms[(previousGeneratedRoom.getRoomy() + 1) * LEVEL_WIDTH + previousGeneratedRoom.getRoomx()] == null){
                        Room room = new Room(zone, this, false, true, false, false, previousGeneratedRoom.getRoomx(), previousGeneratedRoom.getRoomy() + 1);
                        rooms[room.getRoomy() * LEVEL_WIDTH + room.getRoomx()] = room;
                        toAdd.add(room);
                        toRemove.add(previousGeneratedRoom);
                    }
                }
                if (previousGeneratedRoom.southExit){
                    if (rooms[(previousGeneratedRoom.getRoomy() - 1) * LEVEL_WIDTH + previousGeneratedRoom.getRoomx()] == null) {
                        Room room = new Room(zone, this, true, false, false, false, previousGeneratedRoom.getRoomx(), previousGeneratedRoom.getRoomy() - 1);
                        rooms[room.getRoomy() * LEVEL_WIDTH + room.getRoomx()] = room;
                        toAdd.add(room);
                        toRemove.add(previousGeneratedRoom);
                    }
                }
                if (previousGeneratedRoom.westExit){
                    if (rooms[previousGeneratedRoom.getRoomy()* LEVEL_WIDTH + (previousGeneratedRoom.getRoomx() - 1)] == null) {
                        Room room = new Room(zone, this, false, false, false, true, previousGeneratedRoom.getRoomx() - 1, previousGeneratedRoom.getRoomy());
                        rooms[room.getRoomy() * LEVEL_WIDTH + room.getRoomx()] = room;
                        toAdd.add(room);
                        toRemove.add(previousGeneratedRoom);
                    }
                }
                if (previousGeneratedRoom.eastExit){
                    if (rooms[previousGeneratedRoom.getRoomy() * LEVEL_WIDTH + (previousGeneratedRoom.getRoomx() +1)] == null) {
                        Room room = new Room(zone, this, false, false, true, false, previousGeneratedRoom.getRoomx() + 1, previousGeneratedRoom.getRoomy());
                        rooms[room.getRoomy() * LEVEL_WIDTH + room.getRoomx()] = room;
                        toAdd.add(room);
                        toRemove.add(previousGeneratedRoom);
                    }
                }
            }
            previousGeneratedRooms.removeAll(toRemove);
            previousGeneratedRooms.addAll(toAdd);


            for (Room room : toAdd) {
                zoneRooms.add(room);
            }

            toRemove.clear();
            toAdd.clear();

            //shuffle(previousGeneratedRooms);

            GENERATE_EXITS:
            if (globalCost > 0) {
                for (Room previousGeneratedRoom : previousGeneratedRooms) {

                    Connections connections = new Connections(globalCost);

                    validateConnections(connections, previousGeneratedRoom.getRoomx(), previousGeneratedRoom.getRoomy());

                    previousGeneratedRoom.addConnections(connections);
                    //System.out.println("globalCost: " + globalCost + " createdCost:" + connections.getCost());

                    globalCost -= previousGeneratedRoom.getCost();

                    if (globalCost < 1) {
                        break GENERATE_EXITS;
                    }

                }
            }else{
                previousGeneratedRooms.clear();
            }

        }

        generateRoom(zone,0,0,previousGeneratedRooms,globalCost, null);

    }

    private void validateConnections(Connections connections, int roomx, int roomy) {
        if (connections.north && getRoomAt(roomx, roomy +1) != null){
            connections.north = false;
            //System.out.println("removed north");
        }
        if (connections.south && getRoomAt(roomx, roomy -1) != null){
            connections.south = false;
            //System.out.println("removed south");
        }
        if (connections.east && getRoomAt(roomx +1, roomy) != null){
            connections.east = false;
           // System.out.println("removed east");
        }
        if (connections.west && getRoomAt(roomx-1, roomy) != null){
            connections.west = false;
            //System.out.println("removed west");
        }

        connections.generateCost();
    }

    private boolean isFirstRun(int x, int y) {
        return x != 0 && y != 0;
    }

    public Connections generateConnections(int maxConnections){
        Connections connections;
        if (maxConnections < 1){
            return null;
        }
        do {
            connections = new Connections(maxConnections);
        } while (connections.getCost() == 0);

        return connections;
    }


    public void render(SpriteBatch spriteBatch) {
        currentRoom.render(spriteBatch);

        player.render(spriteBatch);
    }

    public void tick(float delta) {

        if (currentRoom != null) {
            currentRoom.tick(delta);
        }

        if (player != null) {
            player.tick(delta);
        }

    }

    public Room getCurrentRoom() {
        return currentRoom;
    }


    public void changeRoom(Entity entity) {
        //System.out.println(currentRoom.getCounter());
        Direction direction = currentRoom.whichRoomChangeDirection(entity);
        int roomx = currentRoom.getRoomx();
        int roomy = currentRoom.getRoomy();
        switch(direction){
            case NORTH:
                currentRoom = rooms[(roomy + 1) * LEVEL_WIDTH + roomx];
                player.getPosition().set(320/2, 40);
                break;
            case SOUTH:
                currentRoom = rooms[(roomy - 1) * LEVEL_WIDTH + roomx];
                player.getPosition().set(320/2, 180);
                break;
            case EAST:
                currentRoom = rooms[roomy * LEVEL_WIDTH + (roomx+1)];
                player.getPosition().set(40, 240/2);
                break;
            case WEST:
                currentRoom = rooms[roomy * LEVEL_WIDTH + (roomx-1)];
                player.getPosition().set(280, 240/2);
                break;
            case NONE:
                break;
        }

        //System.out.println(currentRoom.getCounter());
    }

    public Entity getPlayer() {
        return player;
    }

    public float attack(Entity entity) {

        float attackTimeout = 0.5f;
        if (entity.isPlayer()) {

            Vector2 v = new Vector2();
            PlayerBehaviour behaviour = (PlayerBehaviour) entity.getBehaviour();


            switch (behaviour.getFacingDirection()) {
                case NORTH:
                    v.set(0, 1);
                    break;
                case SOUTH:
                    v.set(0, -1);
                    break;
                case EAST:
                    v.set(1, 0);
                    break;
                case WEST:
                    v.set(-1, 0);
                    break;
                case NONE:
                    v.set(0, 1);
                    break;
            }

            if (behaviour.hasEquipped(AxeBehaviour.class)) {
                currentRoom.addEntityAtPosition(LD36.textures.weaponsAtlas, "axe", new AxeBehaviour(this), 10, 10, entity.getPosition().x, entity.getPosition().y);
                LD36.audio.axe_swing.play();
            }else if (behaviour.hasEquipped(SpearBehaviour.class)){
                currentRoom.addEntityAtPosition(LD36.textures.weaponsAtlas, "spear", new SpearBehaviour(this, v), 10, 10, entity.getPosition().x, entity.getPosition().y);
                LD36.audio.spear_throw.play();
                attackTimeout = 1.0f;
            }else if (behaviour.hasEquipped(SlingshotBehaviour.class)) {
                //System.out.println(behaviour);
                currentRoom.addEntityAtPosition(LD36.textures.stoneAtlas, "stone", new StoneBehaviour(this, v), 10, 10, entity.getPosition().x, entity.getPosition().y);
                LD36.audio.slingshot_throw.play();
                attackTimeout = 0.5f;
            }else{
                message = "I  N E E D  T O  F I N D  A  W E A P O N  F I R S T";
            }
        }

        return attackTimeout;
    }

    public void setGameFinished() {
        gameFinished = true;
    }


    public boolean isGameFinished() {
        return gameFinished;
    }

    public boolean hasMessage() {
        return message != null && !message.isEmpty();
    }

    public String getMessage() {
        return message;
    }

    public void clearMessage(){
        message = null;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
