package st.rhapsody.ld36.level;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by nicklas on 27/08/16.
 */
public class Tile {

    private Sprite sprite = new Sprite();
    private Vector2 position = new Vector2();
    private Rectangle rectangle = new Rectangle();
    private boolean collidable = false;

    public Tile(Sprite sprite, int x, int y) {
        this(sprite, x, y, Color.WHITE, true);
    }

    public Tile(Sprite sprite, int x, int y, Color tint, boolean collidable) {
        this.sprite = sprite;
        this.position.set(x * 16,y * 16);
        this.sprite.setColor(tint);
        this.rectangle.setSize(16,16);
        this.collidable = collidable;

    }

    public void render(SpriteBatch spriteBatch) {

        sprite.draw(spriteBatch);
    }

    public void tick(float delta){

        sprite.setPosition(position.x, position.y);
        this.rectangle.setPosition(position.x, position.y);

    }

    public Vector2 getPosition() {
        return position;
    }

    public boolean isCollidable() {
        return collidable;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }
}
