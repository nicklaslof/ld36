package st.rhapsody.ld36.resource;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by nicklas on 29/08/16.
 */
public class Audio {

    private final AssetManager assetManager;
    public static Sound axe_swing;
    public static  Sound boss_dead;
    public static  Sound mob_dead;
    public static  Sound mob_hurt;
    public static  Sound pickup_artifact;
    public static  Sound pickup_heart;
    public static  Sound pickup_weapon;
    public static  Sound player_hurt;
    public static  Sound slingshot_throw;
    public static  Sound spear_throw;
    public static  Sound ld36_titlesong;
    public static  Sound ld36_song;


    public Audio() {

        assetManager = new AssetManager();

        assetManager.load("axe_swing.ogg", Sound.class);
        assetManager.load("boss_dead.ogg", Sound.class);
        assetManager.load("mob_dead.ogg", Sound.class);
        assetManager.load("mob_hurt.ogg", Sound.class);
        assetManager.load("pickup_artifact.ogg", Sound.class);
        assetManager.load("pickup_heart.ogg", Sound.class);
        assetManager.load("pickup_weapon.ogg", Sound.class);
        assetManager.load("player_hurt.ogg", Sound.class);
        assetManager.load("slingshot_throw.ogg", Sound.class);
        assetManager.load("spear_throw.ogg", Sound.class);
        assetManager.load("ld36_titlesong.ogg", Sound.class);
        assetManager.load("ld36_song.ogg", Sound.class);
        assetManager.finishLoading();


        axe_swing = assetManager.get("axe_swing.ogg", Sound.class);
        boss_dead = assetManager.get("boss_dead.ogg", Sound.class);
        mob_dead = assetManager.get("mob_dead.ogg", Sound.class);
        mob_hurt = assetManager.get("mob_hurt.ogg", Sound.class);
        pickup_artifact = assetManager.get("pickup_artifact.ogg", Sound.class);
        pickup_heart = assetManager.get("pickup_heart.ogg", Sound.class);
        pickup_weapon = assetManager.get("pickup_weapon.ogg", Sound.class);
        player_hurt = assetManager.get("player_hurt.ogg", Sound.class);
        slingshot_throw = assetManager.get("slingshot_throw.ogg", Sound.class);
        spear_throw = assetManager.get("spear_throw.ogg", Sound.class);
        ld36_titlesong = assetManager.get("ld36_titlesong.ogg", Sound.class);
        ld36_song = assetManager.get("ld36_song.ogg", Sound.class);



    }
}
