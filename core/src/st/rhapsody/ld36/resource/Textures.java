package st.rhapsody.ld36.resource;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Created by nicklas on 27/08/16.
 */
public class Textures {


    public static Texture testTexture;

    public static Texture noiseTexture;
    public static Texture twoTexture;
    public static Texture solidTexture;
    public static TextureAtlas heroAtlas;
    public static TextureAtlas plainsAtlas;
    public static TextureAtlas cavesAtlas;
    public static TextureAtlas forestAtlas;
    public static TextureAtlas weaponsAtlas;
    public static TextureAtlas snakeAtlas;
    public static TextureAtlas scorpionAtlas;
    public static Texture stoneTexture;
    public static TextureAtlas stoneAtlas;
    public static Texture heartTexture;
    public static TextureAtlas heartAtlas;
    public static TextureAtlas foxAtlas;
    public static TextureAtlas batAtlas;
    public static TextureAtlas mammothAtlas;
    public static TextureAtlas sabreAtlas;
    public static TextureAtlas artifactsAtlas;
    public static  TextureAtlas ufoAtlas;

    public Textures() {

        // FIX ASSETLOADING

        testTexture = loadTexture("wtf.png");
        noiseTexture = loadTexture("noise.png");
        twoTexture = loadTexture("two.png");
        solidTexture = loadTexture("solid.png");



        heroAtlas = loadTextureAtlas("hero_sprites.pack");
        plainsAtlas = loadTextureAtlas("plains.pack");
        forestAtlas = loadTextureAtlas("forest.pack");
        cavesAtlas = loadTextureAtlas("caves.pack");
        weaponsAtlas = loadTextureAtlas("weapons.pack");
        snakeAtlas = loadTextureAtlas("snake.pack");
        scorpionAtlas = loadTextureAtlas("scorpion.pack");
        foxAtlas = loadTextureAtlas("fox.pack");
        batAtlas = loadTextureAtlas("bat.pack");
        mammothAtlas = loadTextureAtlas("mammoth.pack");
        sabreAtlas = loadTextureAtlas("sabre.pack");
        artifactsAtlas = loadTextureAtlas("artifacts.pack");
        ufoAtlas = loadTextureAtlas("ufo.pack");


        stoneAtlas = loadTextureAtlas("stone.pack");
        heartAtlas = loadTextureAtlas("heart.pack");



    }


    private Texture loadTexture(String textureName){

        FileHandle fileHandle = Gdx.files.internal(textureName);

        Texture texture = new Texture(fileHandle);
        texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        return texture;
    }

    private TextureAtlas loadTextureAtlas(String textureName){
        FileHandle fileHandle = Gdx.files.internal(textureName);
        TextureAtlas textureAtlas = new TextureAtlas(fileHandle);

        return textureAtlas;
    }

}
