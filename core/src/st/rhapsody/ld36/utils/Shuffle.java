package st.rhapsody.ld36.utils;

import java.util.*;

import static java.util.Collections.swap;

/**
 * Created by nicklas on 27/08/16.
 */
public class Shuffle {
    public static void shuffle(List<?> list) {
        Random random = new Random(list.size());

        for(int index = 0; index < list.size(); index += 1) {
            Collections.swap(list, index, index + random.nextInt(list.size() - index));
        }
    }
}
