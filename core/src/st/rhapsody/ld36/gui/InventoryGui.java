package st.rhapsody.ld36.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.entity.Entity;
import st.rhapsody.ld36.entity.behaviour.*;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 27/08/16.
 */
public class InventoryGui extends Gui{


    private final Sprite axe;
    private final Sprite spear;
    private final Sprite slingshot;
    private final Sprite compass;
    private final Sprite steeringwheel;
    private final Sprite map;
    private Level level;

    public InventoryGui(Level level) {

        this.level = level;
        axe = new Sprite(LD36.textures.weaponsAtlas.findRegion("axe"));
        spear = new Sprite(LD36.textures.weaponsAtlas.findRegion("spear"));
        slingshot = new Sprite(LD36.textures.weaponsAtlas.findRegion("slingshot"));



        compass = new Sprite(LD36.textures.artifactsAtlas.findRegion("compass"));
        steeringwheel = new Sprite(LD36.textures.artifactsAtlas.findRegion("steeringwheel"));
        map = new Sprite(LD36.textures.artifactsAtlas.findRegion("map"));



        float startX = 136f;
        axe.setPosition(startX,0);
        spear.setPosition(startX+16,0);
        slingshot.setPosition(startX+32,0);


        startX = 256f;
        compass.setPosition(startX,0);
        steeringwheel.setPosition(startX+20,0);
        map.setPosition(startX+40,0);



    }

    @Override
    protected void doRender(SpriteBatch spriteBatch) {


        Entity player = level.getPlayer();

        PlayerBehaviour playerBehaviour = (PlayerBehaviour) player.getBehaviour();


        drawItem(spriteBatch, playerBehaviour, AxeBehaviour.class, axe);
        drawItem(spriteBatch, playerBehaviour, SpearBehaviour.class, spear);
        drawItem(spriteBatch, playerBehaviour, SlingshotBehaviour.class, slingshot);




        drawItem(spriteBatch, playerBehaviour, CompassBehaviour.class, compass);
        drawItem(spriteBatch, playerBehaviour, SteeringWhellBehaviour.class, steeringwheel);
        drawItem(spriteBatch, playerBehaviour, MapBehaviour.class, map);

    }

    private void drawItem(SpriteBatch spriteBatch, PlayerBehaviour playerBehaviour, Class itemClass, Sprite sprite) {
        if (playerBehaviour.hasItem(itemClass)){
            sprite.setColor(Color.WHITE);
        }else{
            sprite.setColor(Color.DARK_GRAY);
        }

        if (playerBehaviour.hasEquipped(itemClass)){
            sprite.setColor(Color.YELLOW);
        }

        sprite.draw(spriteBatch);
    }

    @Override
    protected void doTick(float delta) {

    }
}
