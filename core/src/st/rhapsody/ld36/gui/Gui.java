package st.rhapsody.ld36.gui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by nicklas on 27/08/16.
 */
public abstract class Gui {

    public void tick(float delta){
        doTick(delta);
    }

    public void render(SpriteBatch spriteBatch){
        doRender(spriteBatch);
    }

    protected abstract void doRender(SpriteBatch spriteBatch);

    protected abstract void doTick(float delta);


}
