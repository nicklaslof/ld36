package st.rhapsody.ld36.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.entity.Entity;
import st.rhapsody.ld36.entity.behaviour.Behaviour;
import st.rhapsody.ld36.entity.behaviour.HealthPickupBehaviour;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 27/08/16.
 */
public class ScoreGui extends Gui{


    private final Sprite heart;
    private Level level;

    public ScoreGui(Level level){
        this.level = level;
        heart = new Sprite(LD36.textures.heartAtlas.findRegion("heart"));
    }

    @Override
    protected void doRender(SpriteBatch spriteBatch){

        Entity player = level.getPlayer();

        Behaviour behaviour = player.getBehaviour();

        int health = behaviour.getHealth();
        int maxHealth = behaviour.getMaxHealth();

        for (int i = 0; i < maxHealth; i++) {
            heart.setPosition(1 + (16*i), 225);

            heart.setColor(Color.DARK_GRAY);
            heart.draw(spriteBatch);
        }



        for (int i = 0; i < health; i++) {
            heart.setPosition(1 + (16*i), 225);

            heart.setColor(HealthPickupBehaviour.heartColor);
            heart.draw(spriteBatch);
        }

    }

    @Override
    protected void doTick(float delta) {

    }
}
