package st.rhapsody.ld36.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld36.entity.behaviour.Behaviour;

/**
 * Created by nicklas on 27/08/16.
 */
public class Entity {

    private final Sprite sprite;
    private Animation upAnimation;
    private TextureAtlas.AtlasRegion idleTexture;
    private int width;
    private int height;
    private Animation animation;
    private boolean animated;
    private TextureAtlas textureAtlas;
    private String[] animations;
    private Vector2 position = new Vector2();
    private Behaviour behaviour;
    private float stateTime;
    private TextureRegion currentFrame;
    private Rectangle rectangle = new Rectangle();

    private boolean isPlayer;
    private boolean disposed;

    public Entity(int x, int y, Texture texture) {
        this.position = position.set(x,y);
        this.sprite = new Sprite(texture);
    }

    public Entity(float x, float y, TextureAtlas.AtlasRegion atlasRegion) {
        this.position = position.set(x,y);
        this.sprite = new Sprite(atlasRegion);
        this.rectangle.setSize(16,16);
        this.width = 16;
        this.height = 16;
    }

    public Entity(float x, float y, TextureAtlas textureAtlas, String idle, String[] animations, String[] upAnimations, int width, int height) {
        this.textureAtlas = textureAtlas;
        this.animations = animations;
        this.position = position.set(x,y);
        this.sprite = new Sprite();
        sprite.setSize(width, height);
        //sprite.setScale(scale);
        this.animated = true;

        this.rectangle.setSize(width,height);

        this.width = width;
        this.height = height;


        Array<TextureRegion> frames = new Array<TextureRegion>();

        for (String animation : animations) {
            TextureAtlas.AtlasRegion region = textureAtlas.findRegion(animation);
            frames.add(region);
        }

        animation = new Animation(0.1f, frames);


        Array<TextureRegion> framesUp = new Array<TextureRegion>();

        for (String animation : upAnimations) {
            TextureAtlas.AtlasRegion region = textureAtlas.findRegion(animation);
            framesUp.add(region);
        }

        upAnimation = new Animation(0.2f, framesUp);


        idleTexture = textureAtlas.findRegion(idle);

    }

    public Vector2 getPosition() {
        return position;
    }

    public Entity setBehaviour(Behaviour behaviour){
        this.behaviour = behaviour;
        this.behaviour.setEntity(this);
        behaviour.setupPrerender(sprite);
        return this;
    }

    public void tick(float delta){
        if (behaviour != null){
            behaviour.tick(delta);
            position.add(behaviour.getPositionTranslation());


            if (behaviour.isIdle()){
                currentFrame = idleTexture;
            }

            if (behaviour.isMoving()){
                if (animated){
                    stateTime += delta;
                    if (behaviour.isWalkingUp()){
                        currentFrame = upAnimation.getKeyFrame(stateTime, true);
                    }else {
                        currentFrame = animation.getKeyFrame(stateTime, true);
                    }
                }
            }


        }

        rectangle.setPosition(position.x, position.y);
        sprite.setPosition(position.x, position.y);
    }

    public void render(SpriteBatch spriteBatch){

        behaviour.preRender(sprite);

        if (sprite.getY() == 0 || sprite.getX() == 0){
            return;
        }

        if (animated){
            if (currentFrame != null) {
                sprite.setRegion(currentFrame);

                if (behaviour != null){
                    sprite.setFlip(!behaviour.isFlipped(), false);
                }

                sprite.draw(spriteBatch);
            }
        }else{
            sprite.draw(spriteBatch);
        }


    }

    public void setPlayer(boolean player) {
        isPlayer = player;
    }

    public boolean isPlayer() {
        return isPlayer;
    }

    public float getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void collided(Entity entity) {
        behaviour.collided(entity);
    }

    public void pickup(Class behaviour) {
        this.behaviour.pickup(behaviour);
    }

    public void setDisposed() {
        this.disposed = true;
    }

    public boolean isDisposed() {
        return disposed;
    }

    public Behaviour getBehaviour() {
        return behaviour;
    }

    public void hurt(int ammount) {
        behaviour.hurt(ammount);
    }
}
