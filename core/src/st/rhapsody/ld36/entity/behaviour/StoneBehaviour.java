package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class StoneBehaviour extends SpearBehaviour {
    public StoneBehaviour(Level level, Vector2 castDirection) {
        super(level, castDirection);
        ttl = 0.5f;
        damage = 1;
    }

    @Override
    public void setupPrerender(Sprite sprite) {
        super.setupPrerender(sprite);
        sprite.setColor(Color.LIGHT_GRAY);
    }

    @Override
    public void preRender(Sprite sprite) {

    }
}
