package st.rhapsody.ld36.entity.behaviour;

import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class SteeringWhellBehaviour extends ArtifactBehaviour {
    public SteeringWhellBehaviour(Level level) {
        super(level, SteeringWhellBehaviour.class);
    }
}