package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld36.level.Level;
import st.rhapsody.ld36.level.Room;
import st.rhapsody.ld36.level.Tile;

/**
 * Created by nicklas on 27/08/16.
 */
public class MovingBehaviour extends Behaviour{

    private Vector2 calcVector2 = new Vector2();
    private Rectangle calcRectangle = new Rectangle();

    protected boolean moving;
    protected boolean idle;
    protected boolean flipped;
    protected boolean walkingUp;

    public MovingBehaviour(Level level) {
        super(level);
    }


    @Override
    protected void doTick(float delta) {

    }


    protected boolean canMove(Vector2 newPosition, Vector2 position){

        Vector2 posToCheck = calcVector2.set(position).add(newPosition);

        calcRectangle.setPosition(posToCheck.x, posToCheck.y);
        calcRectangle.setSize(entity.getWidth(), entity.getHeight());

        Room currentRoom = this.level.getCurrentRoom();


        Tile[] tiles = currentRoom.getTiles();

        for (Tile tile : tiles) {
            if (tile != null && tile.isCollidable()){
                if (calcRectangle.overlaps(tile.getRectangle())){
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean isMoving() {
        return moving;
    }

    @Override
    public boolean isIdle() {
        return idle;
    }

    @Override
    public boolean isFlipped() {
        return flipped;
    }

    @Override
    public boolean isWalkingUp() {
        return walkingUp;
    }
}
