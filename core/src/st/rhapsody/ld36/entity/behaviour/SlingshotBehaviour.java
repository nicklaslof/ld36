package st.rhapsody.ld36.entity.behaviour;

import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class SlingshotBehaviour extends WeaponBehaviour{
    public SlingshotBehaviour(Level level, float ttl) {
        super(level, ttl, 8);
    }
}
