package st.rhapsody.ld36.entity.behaviour;

import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.entity.Entity;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class BossBehaviour extends EnemyBehaviour {

    private String artifactName;




    public BossBehaviour(Level level, int speed, String artifactName, int health) {
        super(level,speed, health);
        this.artifactName = artifactName;

    }

    @Override
    public void tick(float delta) {
        super.tick(delta);

        //System.out.println(health);
        if (health <= 0){

            //System.out.println(artifactName);
            LD36.audio.boss_dead.play();

            ArtifactBehaviour artifactBehaviour = null;

            if (artifactName.equals("compass")){
                artifactBehaviour = new CompassBehaviour(level);
            }else if (artifactName.equals("steeringwheel")){
                artifactBehaviour = new SteeringWhellBehaviour(level);
            }else if (artifactName.equals("map")){
                artifactBehaviour = new MapBehaviour(level);
            }

            level.getCurrentRoom().addEntity(LD36.textures.artifactsAtlas, artifactName, artifactBehaviour, null, null, null, 16, 16,
                    entity.getPosition().x, entity.getPosition().y);
        }
    }

    @Override
    public void collided(Entity entity) {
        super.collided(entity);


    }
}

