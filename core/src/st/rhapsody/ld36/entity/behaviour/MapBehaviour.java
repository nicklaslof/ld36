package st.rhapsody.ld36.entity.behaviour;

import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class MapBehaviour extends ArtifactBehaviour {
    public MapBehaviour(Level level) {
        super(level, MapBehaviour.class);
    }
}