package st.rhapsody.ld36.entity.behaviour;

import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class WeaponBehaviour extends Behaviour{
    protected final Level level;
    protected float ttl;
    private float alive = 0.0f;

    protected int damage = 1;

    public WeaponBehaviour(Level level, float ttl, int damage) {
        super(level);
        this.level = level;
        this.ttl = ttl;
        this.damage = damage;
    }

    @Override
    protected void doTick(float delta) {
        if (ttl > 0) {
            alive += delta;

            if (alive >= ttl) {
                entity.setDisposed();
            }
        }
    }

    public int getDamage() {
        return damage;
    }
}
