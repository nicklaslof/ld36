package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import st.rhapsody.ld36.entity.Entity;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class PickupBehaviour extends Behaviour{
    private final Class realBehaviour;
    protected boolean changeColor = false;
    private float timeElapsed = 0f;
    public PickupBehaviour(Level level, Class realBehaviour) {
        super(level);
        this.realBehaviour = realBehaviour;
    }

    @Override
    protected void doTick(float delta) {
        timeElapsed += delta;

        if (timeElapsed > 0.01f){
            changeColor = true;
        }

        if (timeElapsed > 0.06f){
            changeColor = false;
            timeElapsed = 0f;
        }
    }

    @Override
    public void setupPrerender(Sprite sprite) {
        super.setupPrerender(sprite);
        sprite.setColor(Color.BLACK);
    }

    @Override
    public void preRender(Sprite sprite) {
        if (changeColor){
            sprite.setColor(Color.WHITE);
        }else{
            sprite.setColor(Color.BLACK);
        }
    }

    @Override
    public void collided(Entity collidedEntity) {
        super.collided(collidedEntity);
        if (collidedEntity.isPlayer()){
            collidedEntity.pickup(realBehaviour);
            entity.setDisposed();
        }
    }
}
