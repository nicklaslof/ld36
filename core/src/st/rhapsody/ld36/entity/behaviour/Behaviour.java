package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld36.entity.Entity;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 27/08/16.
 */
public abstract class Behaviour {

    protected Vector2 positionTranslation = new Vector2();
    protected Entity entity;
    protected Level level;

    public Behaviour(Level level) {
        this.level = level;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public void tick(float delta){
        positionTranslation.set(0,0);
        doTick(delta);
    }

    public Vector2 getPositionTranslation() {
        return positionTranslation;
    }

    protected abstract void doTick(float delta);

    public boolean isIdle() {
        return false;
    }

    public boolean isMoving() {
        return false;
    }

    public boolean isFlipped(){
        return false;
    }

    public boolean isWalkingUp() {
        return false;
    }

    public void collided(Entity entity) {

    }

    public void preRender(Sprite sprite){

    }

    public void setupPrerender(Sprite sprite) {

    }

    public void pickup(Class behaviour) {

    }

    public int getHealth() {
        return 0;
    }

    public void hurt(int ammount) {

    }

    public int getMaxHealth() {
        return 0;
    }
}
