package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.entity.Entity;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class UfoBehaviour extends Behaviour{
    private boolean flyAway;
    private float totalTime;

    public UfoBehaviour(Level level) {
        super(level);
    }

    @Override
    protected void doTick(float delta) {

        totalTime += delta*4;

        if (flyAway){

            float cos = MathUtils.cos(totalTime) /2f;

            positionTranslation.set(cos,delta*10);


            level.getPlayer().getPosition().set(entity.getPosition().x+4, entity.getPosition().y+12);

            if (entity.getPosition().y > 240){
                level.setGameFinished();
                LD36.changeToEndScreen();
            }

        }

    }

    @Override
    public boolean isMoving() {
        return true;
    }


    @Override
    public void collided(Entity entity) {
        if (entity.isPlayer() && !entity.isDisposed()){
            PlayerBehaviour behaviour = (PlayerBehaviour) entity.getBehaviour();


            int artifacts = 0;

            if (behaviour.hasItem(CompassBehaviour.class)) artifacts++;
            if (behaviour.hasItem(SteeringWhellBehaviour.class)) artifacts++;
            if (behaviour.hasItem(MapBehaviour.class)) artifacts++;

            if (artifacts == 3){
                flyAway = true;
                level.getPlayer().setDisposed();
                LD36.audio.ld36_song.loop();
            }else{
                level.setMessage(" I  N E E D  A L L  A R T I F A C T S  F I R S T");
            }


        }
    }
}
