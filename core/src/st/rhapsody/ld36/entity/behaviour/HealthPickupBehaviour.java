package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class HealthPickupBehaviour extends PickupBehaviour{

    public static Color heartColor = new Color(0xa63014ff);

    public HealthPickupBehaviour(Level level) {
        super(level, HealthPickupBehaviour.class);
    }

    @Override
    public void preRender(Sprite sprite) {
        sprite.setColor(heartColor);
    }
}
