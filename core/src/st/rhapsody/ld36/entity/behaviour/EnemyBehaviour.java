package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.entity.Entity;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class EnemyBehaviour extends MovingBehaviour{
    private final int speed;
    private float spawnX;
    private float spawnY;
    protected int health;
    private final int maxHealth;
    private float hurtCountdown = 0.0f;

    private Vector2 tmpVector = new Vector2();
    private boolean hit;

    public EnemyBehaviour(Level level, int speed, int health) {
        super(level);
        this.speed = speed;
        this.health = health;
        this.maxHealth = health;
    }

    @Override
    public void preRender(Sprite sprite) {
        super.preRender(sprite);
        if (hit){
            sprite.setColor(Color.RED);
            if (hurtCountdown == 0){
                hit = false;
            }
        }else{
            sprite.setColor(Color.BLACK);
        }
    }

    @Override
    public void tick(float delta) {
        super.tick(delta);
        if (health <= 0){
            this.entity.setDisposed();
            if (!this.getClass().getName().equals(BossBehaviour.class.getName())){
                LD36.audio.mob_dead.play();
            }
            if (MathUtils.random(1, 100) < 25) {
                level.getCurrentRoom().addEntity(LD36.textures.heartAtlas, "heart", new HealthPickupBehaviour(level), null, null, null, 16, 16, entity.getPosition().x, entity.getPosition().y);
            }
        }

        hurtCountdown -= delta;

        if (hurtCountdown < 0.0f){
            hurtCountdown = 0.0f;
        }

        this.moving = true;
        this.idle = true;

        if (spawnX == 0 && spawnY == 0){
            spawnX = entity.getPosition().x;
            spawnY = entity.getPosition().y;
        }

        if (!level.getPlayer().isDisposed()) {
            tmpVector.set(level.getPlayer().getPosition());
            tmpVector.sub(entity.getPosition());
            if (tmpVector.x > -0.4f && tmpVector.x < 0.4f) {
                tmpVector.x = 0;
            }

            float aggroRadius = 70;

            if (this instanceof BossBehaviour) {
                aggroRadius = 160;
            }

            if (tmpVector.x < -aggroRadius || tmpVector.x > aggroRadius || tmpVector.y < -aggroRadius || tmpVector.y > aggroRadius) {
                this.idle = true;
                this.moving = false;
                return;
            }

            tmpVector.nor();

            tmpVector.scl(speed * delta);


            if (tmpVector.x > 0.2f) {
                flipped = true;
            } else {
                flipped = false;
            }

            positionTranslation.set(tmpVector);
        }
    }

    @Override
    public void collided(Entity entity) {
        super.collided(entity);
        if (entity.isPlayer()){
            entity.hurt(1);
        }else{
            if (hurtCountdown == 0.0f) {
                if (entity.getBehaviour() instanceof WeaponBehaviour) {
                    WeaponBehaviour weaponBehaviour = (WeaponBehaviour) entity.getBehaviour();
                    health -= weaponBehaviour.getDamage();
                    hurtCountdown = 0.3f;
                    hit = true;
                    LD36.audio.mob_hurt.play();
                }
            }
            if (entity.getBehaviour() instanceof WeaponBehaviour) {
                entity.setDisposed();
            }
        }
    }
}