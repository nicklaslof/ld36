package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld36.level.Level;
import st.rhapsody.ld36.level.Room;
import st.rhapsody.ld36.level.Tile;

/**
 * Created by nicklas on 28/08/16.
 */
public class SpearBehaviour extends WeaponBehaviour {
    private float currentSpeed = 0.0f;
    private Vector2 castDirection;
    private Vector2 tmpVector = new Vector2();

    private Vector2 calcVector2 = new Vector2();
    private Rectangle calcRectangle = new Rectangle();

    private boolean flipSprite = false;

    public SpearBehaviour(Level level, Vector2 castDirection) {
        super(level, 1, 2);
        this.castDirection = castDirection;
        this.castDirection.nor();
        currentSpeed = 700.0f;
    }

    @Override
    public void preRender(Sprite sprite) {
        super.preRender(sprite);

        if (flipSprite){
            sprite.setFlip(true, false);
        }
    }

    @Override
    public void tick(float delta) {
        super.tick(delta);
        positionTranslation.set(0,0);

        if (currentSpeed > 0.0f) {
            currentSpeed = currentSpeed / 1.1f;

            if (currentSpeed < 0.1f) {
                currentSpeed = 0.0f;
            }

            if (currentSpeed < 2.0f){
                flipSprite = true;
            }else{
                flipSprite = false;
            }

            //System.out.println(currentSpeed);

            tmpVector.set(castDirection).scl(currentSpeed*delta);

            if (canMove(tmpVector, entity.getPosition())) {

                positionTranslation.set(tmpVector);
            }
        }
    }
    protected boolean canMove(Vector2 newPosition, Vector2 position){

        Vector2 posToCheck = calcVector2.set(position).add(newPosition);

        if (posToCheck.x < 16 || posToCheck.x > (Room.TILE_WIDTH*16)-16 || posToCheck.y < 32 || posToCheck.y > (Room.TILE_HEIGHT*16)-32){
            return false;
        }

        calcRectangle.setPosition(posToCheck.x, posToCheck.y);
        calcRectangle.setSize(entity.getWidth(), entity.getHeight());

        Room currentRoom = this.level.getCurrentRoom();


        Tile[] tiles = currentRoom.getTiles();

        for (Tile tile : tiles) {
            if (tile != null && tile.isCollidable()){
                if (calcRectangle.overlaps(tile.getRectangle())){
                    return false;
                }
            }
        }

        return true;
    }
}
