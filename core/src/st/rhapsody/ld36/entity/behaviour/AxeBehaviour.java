package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 28/08/16.
 */
public class AxeBehaviour extends WeaponBehaviour{


    private Vector2 tmpVector = new Vector2();
    private boolean flipSprite = false;
    public AxeBehaviour(Level level) {
        super(level, 0.1f, 4);
    }

    @Override
    public void preRender(Sprite sprite) {
        //System.out.println(sprite.getX()+" "+sprite.getY());
        if (flipSprite){
            sprite.setFlip(true, false);
        }else{
            sprite.setFlip(false, false);
        }
    }

    @Override
    public void tick(float delta) {
        super.tick(delta);
        Vector2 playerPosition = this.level.getPlayer().getPosition();

        Behaviour behaviour = this.level.getPlayer().getBehaviour();

        tmpVector.set(playerPosition);
        tmpVector.sub(entity.getPosition());

        if (behaviour instanceof PlayerBehaviour){
            PlayerBehaviour playerBehaviour = (PlayerBehaviour) behaviour;


            switch (playerBehaviour.getFacingDirection()){
                case NORTH:
                    flipSprite = false;
                    tmpVector.add(0,10);
                    break;
                case SOUTH:
                    flipSprite = false;
                    tmpVector.add(0,-10);
                    break;
                case EAST:
                    tmpVector.add(10,0);
                    flipSprite = true;
                    break;
                case WEST:
                    flipSprite = false;
                    tmpVector.add(-10, 0);
                    break;
            }
        }


        positionTranslation.set(tmpVector);



    }
}
