package st.rhapsody.ld36.entity.behaviour;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld36.LD36;
import st.rhapsody.ld36.level.Direction;
import st.rhapsody.ld36.level.Level;

/**
 * Created by nicklas on 27/08/16.
 */
public class PlayerBehaviour extends MovingBehaviour{

    private float speed = 100f;

    private float fireCountdown = 0.0f;
    private float hurtCountdown = 0.0f;


    private Direction facingDirection = Direction.NONE;

    private int health = 5;
    private int maxHealth = 5;

    private Array<Class> inventory = new Array<Class>();
    private Class currentWeapon;

    public PlayerBehaviour(Level level) {
        super(level);
    }


    @Override
    protected void doTick(float delta) {
        super.doTick(delta);
        fireCountdown -= delta;

        if (fireCountdown < 0.0f){
            fireCountdown = 0.0f;
        }

        hurtCountdown -= delta;

        if (hurtCountdown < 0.0f){
            hurtCountdown = 0.0f;
        }

        if (health <= 0){
            LD36.changeToGameOverScreen();
        }

        moving = false;
        idle = true;
        flipped = false;
        walkingUp = false;
        if (!entity.isDisposed()) {
            if (Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP)) {
                positionTranslation.add(0, speed * delta);
                facingDirection = Direction.NORTH;
                walkingUp = true;
            }

            if (Gdx.input.isKeyPressed(Input.Keys.S) || Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                positionTranslation.add(0, -speed * delta);
                facingDirection = Direction.SOUTH;
                walkingUp = true;
            }

            if (Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                float walkingSpeed = speed;
                if (walkingUp) {
                    walkingSpeed = speed / 2;
                }
                positionTranslation.add(-walkingSpeed * delta, 0);
                facingDirection = Direction.WEST;
                flipped = true;
            }

            if (Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                float walkingSpeed = speed;
                if (walkingUp) {
                    walkingSpeed = speed / 2;
                }
                positionTranslation.add(walkingSpeed * delta, 0);
                facingDirection = Direction.EAST;
                flipped = false;
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) && fireCountdown == 0.0f) {
                fireCountdown = level.attack(entity);
            }


            if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)) {
                if (hasItem(AxeBehaviour.class)) {
                    currentWeapon = AxeBehaviour.class;
                }
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) {
                if (hasItem(SpearBehaviour.class)) {
                    currentWeapon = SpearBehaviour.class;
                }
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_3)) {
                if (hasItem(SlingshotBehaviour.class)) {
                    currentWeapon = SlingshotBehaviour.class;
                }
            }


            if ((positionTranslation.x != 0f || positionTranslation.y != 0f) && canMove(positionTranslation, entity.getPosition())) {
                moving = true;
                idle = false;
            } else {
                idle = true;
                positionTranslation.set(0, 0);
            }

            level.changeRoom(entity);
        }

    }

    public Direction getFacingDirection() {
        return facingDirection;
    }


    public boolean hasItem(Class clazz){
        for (Class behaviour : inventory) {
           if (behaviour.getName().equals(clazz.getName())){
                return true;
            }
        }

        return false;
    }

    @Override
    public void pickup(Class behaviour) {
        if (behaviour.getName().equals(HealthPickupBehaviour.class.getName())){
            if (health < maxHealth) {
                heal(1);
                LD36.audio.pickup_heart.play();
            }
        }else {
            inventory.add(behaviour);
            if (behaviour.getName().equals(AxeBehaviour.class.getName()) || behaviour.getName().equals(SpearBehaviour.class.getName()) || behaviour.getName().equals(SlingshotBehaviour.class.getName())){
                currentWeapon = behaviour;
                LD36.audio.pickup_weapon.play();
            }else{
                LD36.audio.pickup_artifact.play();
            }
        }
    }

    private void heal(int i) {
        health++;
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public int getMaxHealth() {
        return maxHealth;
    }

    @Override
    public void hurt(int ammount) {
        if (hurtCountdown == 0.0f && MathUtils.random(1,100) < 4) {
            health -= ammount;
            LD36.audio.player_hurt.play();
            if (health < 0){
                health = 0;
            }
            hurtCountdown = 1.0f;
        }
    }

    public boolean hasEquipped(Class itemClass) {
        if (currentWeapon == null){
            return false;
        }
        return currentWeapon.getName().equals(itemClass.getName());
    }
}
